package ivancordasic.ferit.fragmentapp;

public interface SubmitClickListener {
    void onSubmitClick(String message);
}
