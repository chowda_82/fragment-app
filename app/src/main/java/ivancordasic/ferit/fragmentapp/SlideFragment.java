package ivancordasic.ferit.fragmentapp;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 */
public class SlideFragment extends Fragment {

    private static final String BUNDLE_MESSAGE = "Slide";

    public static SlideFragment newInstance(Type arg)
    {
        SlideFragment fragment = new SlideFragment();
        Bundle args = new Bundle();
        args.putSerializable(BUNDLE_MESSAGE, arg);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_slide, container, false);
        setUpFragType(view);
        return view;
    }

    @SuppressLint("ResourceAsColor")
    private void setUpFragType(View view) {
        switch((Type) Objects.requireNonNull(getArguments().get(BUNDLE_MESSAGE))){
            case IMAGE:
                view.findViewById(R.id.tvMessageSlide).setVisibility(View.GONE);
                view.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.white));
                break;
            default: view.findViewById(R.id.ivImage).setVisibility(View.GONE);
        }
    }
}