package ivancordasic.ferit.fragmentapp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class StudentPresenceFragment extends Fragment implements NameClickListener {

    private RecyclerFragmentAdapter adapter;
    private TextView tvName;
    private EditText etName;
    private Button btnAdd;

    public StudentPresenceFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_student_presence, container, false);
        this.tvName = (TextView) view.findViewById(R.id.tvName);
        this.etName = (EditText) view.findViewById(R.id.etName);
        this.btnAdd = (Button) view.findViewById(R.id.btnAdd);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addCell(view);
            }
        });

        RecyclerView recycler = view.findViewById(R.id.rvRecycler);
        recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        this.adapter = new RecyclerFragmentAdapter(this);
        recycler.setAdapter(this.adapter);
        setupRecyclerData();
        return view;
    }



    private void setupRecyclerData() {
        List<String> data = new ArrayList<>();
        data.add("John");
        data.add("Finch");
        this.adapter.addData(data);
    }

    public void addCell(View view) {
        if (this.etName.getText().toString().equals("")) {
            Toast.makeText(getActivity(), "Error! Name needed!!", Toast.LENGTH_SHORT).show();
            return;
        }
        this.adapter.addCell(etName.getText().toString());
        this.etName.setText(null);
        this.etName.setHint(R.string.etNameHint);
    }


    @Override
    public void onNameClick(int position) {
        Toast.makeText(getActivity(),"Error! Name needed!",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRemoveClick(int position) {
        this.adapter.removeCell(position);
        Toast.makeText(getActivity(), "Student at position " + position + " removed!", Toast.LENGTH_SHORT).show();
    }
}
