package ivancordasic.ferit.fragmentapp;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class MessageFragment extends Fragment {

    private static final String BUNDLE_MESSAGE = "Message";

    private TextView tvMessage;

    public MessageFragment() {
        // Required empty public constructor
    }

    public static MessageFragment newInstance(String arg)
    {
        MessageFragment fragment = new MessageFragment();
        Bundle args = new Bundle();
        args.putString(BUNDLE_MESSAGE, arg);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_message, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        tvMessage = view.findViewById(R.id.tvMessage);
        MainActivity.setFragment(this);
    }

    public void displayMessage(String message) {
        tvMessage.setText(!message.trim().isEmpty() ? message : "...");
    }

}
