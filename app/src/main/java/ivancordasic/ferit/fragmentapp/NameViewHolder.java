package ivancordasic.ferit.fragmentapp;


import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class NameViewHolder extends RecyclerView.ViewHolder {

    private TextView tvName;

    public NameViewHolder(@NonNull View itemView, final NameClickListener listener) {
        super(itemView);
        this.tvName = itemView.findViewById(R.id.tvName);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onNameClick(getAdapterPosition());
            }
        });
        ImageView ivRemove = itemView.findViewById(R.id.ivRemove);
        ivRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onRemoveClick(getAdapterPosition());
            }
        });
    }

    public void setName(String name) {
        this.tvName.setText(name);
    }
}
