package ivancordasic.ferit.fragmentapp;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class InputFragment extends Fragment implements TextWatcher {

    private static final String BUNDLE_MESSAGE = "Input";

    private String mMessageString = "...";
    private Button btnSubmit;
    private EditText etMessage;

    private SubmitClickListener submitClickListener;

    public InputFragment() {
        // Required empty public constructor
    }

    public static InputFragment newInstance(String arg)
    {
        InputFragment fragment = new InputFragment();
        Bundle args = new Bundle();
        args.putString(BUNDLE_MESSAGE, arg);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SubmitClickListener) {
            this.submitClickListener = (SubmitClickListener) context;
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_input, container, false);
        btnSubmit = view.findViewById(R.id.btnSubmit);
        etMessage = view.findViewById(R.id.etMessage);

        etMessage.addTextChangedListener(this);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(), mMessageString, Toast.LENGTH_SHORT).show();
                submitClickListener.onSubmitClick(mMessageString);
            }
        });
        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if(!etMessage.toString().trim().isEmpty()) {
            btnSubmit.setEnabled(true);
            mMessageString = etMessage.getText().toString();
        }

    }


}