package ivancordasic.ferit.fragmentapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity implements SubmitClickListener {


    private TabStatePagerAdapter mTabStatePagerAdapter;
    private ViewPager mViewPager;

    private static MessageFragment messageFragment = new MessageFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mTabStatePagerAdapter = new TabStatePagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.viewPager);

        setupViewPager(mViewPager);

    }

    private void setupViewPager(ViewPager mViewPager) {
        TabStatePagerAdapter adapter = new TabStatePagerAdapter(getSupportFragmentManager());
        adapter.addFragment(InputFragment.newInstance("Input"), "Input");
        adapter.addFragment(MessageFragment.newInstance("Message"), "Output");
        adapter.addFragment(new StudentPresenceFragment(), "Student Presence");
        adapter.addFragment(SlideFragment.newInstance(Type.TEXT), "Text Fragment");
        adapter.addFragment(SlideFragment.newInstance(Type.IMAGE), "Image Fragment");
        mViewPager.setAdapter(adapter);
        mViewPager.setPageTransformer(false, new DepthPageTransformer());
    }

    @Override
    public void onSubmitClick(String message) {
        messageFragment.displayMessage(message);
        mViewPager.setCurrentItem(1);
    }

    public static void setFragment(MessageFragment messageFragment) {
        MainActivity.messageFragment = messageFragment;
    }
}