package ivancordasic.ferit.fragmentapp;

public interface NameClickListener {
    void onNameClick(int position);
    void onRemoveClick(int position);
}
