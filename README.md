# Fragment app

Android aplikacija koju sam napravio za zadaću iz kolegija Osnove razvoja web i mobilnih aplikacija. Osim traženih mogućnosti, nadogradio sam aplikciju animacijama prijelaza između tabova, ubacio dodatni fragment i koristio sam RecyclerView u koji sam ubacio prisutnost studenata iz jedne od prošlih aplikacija. Aplikaciju sam testirao na Xiaomi Redmi Note 8T (fizički uređaj) te na Google Pixel 4 (emulator, API 29).

## Screenshotovi aplikacije:

<img src="/uploads/c5793419f35a6ad6e3bc709bbed6ca6c/1.jpeg" width="400" height="800"/>

<img src="/uploads/ce65d74d331ccdc9ce6f8beddf461668/2.jpeg" width="400" height="800"/>

<img src="/uploads/62765f28287fd94c60518d6a8f2ceeb1/3.jpeg" width="400" height="800"/>

<img src="/uploads/51e6d8dea2a57a790a14617037dc9c21/4.jpeg" width="400" height="800"/>

<img src="/uploads/f4ab45866a7351a6d5c79a663ed54c3b/5.jpeg" width="400" height="800"/>
